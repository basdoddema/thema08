library(deSolve)

# Script that implements a mathematical model that calculates the dynamics and actions of Virus,
# Macrophages and epithelial cells

## SET PARAMETERS
# fixed parameters
sig.mr = 1/25
sig.ma = sig.mr
sig.mi = sig.mr
sig.i = 2
rho= 0.5 # was 1
f = 1/3 # was 3
e.1 = 0
e.2 = 0

# variable function for H1N1 virus
parms.H1N1 <- function(b = 8.4e-09, P = 4.6, k = 2.6e+02, P.m = 2.0e-01, alpha= 1.8e-02,
                       gam= 2.2e-03, mu= 2.5e-09, s = 508725.2, v50 = 3.9e-04){
  parms <- c(b = b, P = P, k = k, P.m = P.m, alpha= alpha, gam= gam, mu= mu, s = s, v50 = v50)
  return(parms)
}
# variable function for H5N1 virus
parms.H5N1 <- function(b = 6.0e-09, P = 4.9, k = 2.2e-09, P.m = 5.4e-02, alpha= 5.0e-02,
                       gam= 3.2e-03, mu= 9.7e+01, s = 372997.0, v50 = 3.4e-04){
  parms <- c(b = b, P = P, k = k, P.m = P.m, alpha= alpha, gam= gam, mu= mu, s = s, v50 = v50)
  return(parms)
}

# assign the paramters to variables
parms.h1n1.low <- parms.H1N1(P.m = 0, gam= 0)
parms.h5n1.low <- parms.H5N1(P.m = 0, gam= 0)
parms.h1n1.high <- parms.H1N1(P.m = 1e-09, alpha= 2.2e-01) # P.m changed to 1e-09
parms.h5n1.high <- parms.H5N1(P.m = 1e-09, alpha= 4.9e-01) # P.m changed to 1e-09

# define initial state
state <- c(S = 7e+09, I = 0, V = 1e+02, M.r = 8669664,
           M.a = 0, M.i = 0, A = 0)

# define time sequence for the model 
times <- seq(0, 15, by=0.1)

# define model 
model <- function(t, y, parms) {
  with(as.list(c(parms, y)),{
    dT <- -b * S * V;  #S = T
    dI <- b * S * V - sig.i * I;
    dV <- (1- e.1)*(P * I + P.m * M.i) - (k * A * V)  - (b * S * V) - (f * V); #f = c
    dM.r <- s -(1- e.2) * alpha * V * M.r / (v50 + V) -  sig.mr * M.r;
    dM.a <- (1 - e.2) * alpha * V * M.r / (v50 + V) - gam * M.a * V - sig.ma * M.a;
    dM.i <- gam * M.a * V - sig.mi * M.i;
    dA <- mu * M.a + rho * A
    return(list(c(dT, dI, dV, dM.r, dM.a, dM.i, dA)))
  }
  )
}

# perform ode on LP virusses
model.h1n1.low  <- try(ode(times = times, y = state, parms = parms.h1n1.low, func = model, method = "lsoda"))
model.h5n1.low  <- try(ode(times = times, y = state, parms = parms.h5n1.low, func = model, method = "lsoda"))

# perform ode on HP virusses
model.h1n1.high <- try(ode(times = times, y = state, parms = parms.h1n1.high, func = model, method = "lsoda"))
model.h5n1.high <- try(ode(times = times, y = state, parms = parms.h5n1.high, func = model, method = "lsoda"))


# plot uninfected epithelial cells for H5N1
par(mfrow=c(2,2))
plot(model.h5n1.low[,1], model.h5n1.low[,2], type="l", main="Uninfected epithelial cells",
     ylab="Amount of cells", xlab="Days post infection")
lines(model.h5n1.low[,1], model.h5n1.high[,2], col="red")
legend(x = "bottomleft", col = c("black", "red"), lty = 1,legend = c("LP H5N1", "HP H5N1"))

# plot uninfected epithelial cells for H1N1
plot(model.h1n1.low[,1], model.h1n1.low[,2], type="l", main="Uninfected epithelial cells",
     ylab="Amount of cells", xlab="Days post infection")
lines(model.h1n1.low[,1], model.h1n1.high[,2], col="red")
legend(x = "bottomleft", col = c("black", "red"), lty = 1,legend = c("LP H1N1", "HP H1N1"))

# plot infected epithelial cells for H5N1
plot(model.h5n1.low[,1], model.h5n1.low[,3], type="l", main="Infected epithelial cells",
     ylab="Amount of cells", xlab="Days post infection")
lines(model.h5n1.low[,1], model.h5n1.high[,3], col="red")
legend(x = "topleft", col = c("black", "red"), lty = 1,legend = c("LP H5N1", "HP H5N1"))

# plot infected epithelial cells for H1N1
plot(model.h1n1.low[,1], model.h1n1.low[,3], type="l", main="Infected epithelial cells",
     ylab="Amount of cells", xlab="Days post infection")
lines(model.h1n1.low[,1], model.h1n1.high[,3], col="red")
legend(x = "topleft", col = c("black", "red"), lty = 1,legend = c("LP H1N1", "HP H1N1"))

# plot virus for H5N1
par(mfrow=c(2,2))
plot(model.h5n1.low[,1], model.h5n1.low[,4], type="l", main="Free virus",
     ylab="Amount of virus", xlab="Days post infection")
lines(model.h5n1.low[,1], model.h5n1.high[,4], col="red")
legend(x = "topright", col = c("black", "red"), lty = 1,legend = c("LP H5N1", "HP H5N1"))

# plot virus for H1N1
plot(model.h1n1.low[,1], model.h1n1.low[,4], type="l", main="Free virus",
     ylab="Amount of virus", xlab="Days post infection")
lines(model.h1n1.low[,1], model.h1n1.high[,4], col="red")
legend(x = "topright", col = c("black", "red"), lty = 1,legend = c("LP H1N1", "HP H1N1"))

# plot amount of macrophages for HP H5N1
par(mfrow=c(2,2))
plot(model.h5n1.high[,1], model.h5n1.high[,5],ylim=c(1e1,1e7), type = "l", ylab="Macrophage numbers",
     xlab="Days post infection", main="Macrophages HP H5N1")
lines(model.h5n1.high[,1], model.h5n1.high[,6], col = "blue")
lines(model.h5n1.high[,1], model.h5n1.high[,7], col = "red")
legend(x = "bottomright", col = c("black", "blue", "red"), lty = 1,legend = c("Mr", "Ma", "Mi"))

# plot amount of macrophages for HP H1N1
plot(model.h1n1.high[,1], model.h1n1.high[,5],ylim=c(1e1,1e7), type = "l", ylab="Macrophage numbers",
     xlab="Days post infection", main="Macrophages HP H1N1")
lines(model.h1n1.high[,1], model.h1n1.high[,6], col = "blue")
lines(model.h1n1.high[,1], model.h1n1.high[,7], col = "red")
legend(x = "bottomright", col = c("black", "blue", "red"), lty = 1,legend = c("Mr", "Ma", "Mi"))

# plot amount of macrophages for LP H5N1
plot(model.h5n1.low[,1], model.h5n1.low[,5],ylim=c(1e1,1e7), type = "l", ylab="Macrophage numbers",
     xlab="Days post infection", main="Macrophages LP H5N1")
lines(model.h5n1.low[,1], model.h5n1.low[,6], col = "blue")
lines(model.h5n1.low[,1], model.h5n1.low[,7], col = "red")
legend(x = "bottomright", col = c("black", "blue", "red"), lty = 1,legend = c("Mr", "Ma", "Mi"))

# plot amount of macrophages for LP H1N1
plot(model.h1n1.low[,1], model.h1n1.low[,5],ylim=c(1e1,1e7), type = "l", ylab="Macrophage numbers",
     xlab="Days post infection", main="Macrophages LP H1N1")
lines(model.h1n1.low[,1], model.h1n1.low[,6], col = "blue")
lines(model.h1n1.low[,1], model.h1n1.low[,7], col = "red")
legend(x = "bottomright", col = c("black", "blue", "red"), lty = 1,legend = c("Mr", "Ma", "Mi"))

# plot amount of antibodies for H1N1
par(mfrow=c(2,2))
plot(model.h1n1.low[,1], model.h1n1.low[,8], type="l", main="Antibodies",
     ylab="Amount of antibodies", xlab="Days post infection")
lines(model.h1n1.low[,1], model.h1n1.high[,8], col="red")
legend(x = "topleft", col = c("black", "red"), lty = 1,legend = c("LP H1N1", "HP H1N1"))

# plot amount of antibodies for H5N1
plot(model.h5n1.low[,1], model.h5n1.low[,8], type="l", main="Antibodies",
     ylab="Amount of antibodies", xlab="Days post infection")
lines(model.h5n1.low[,1], model.h5n1.high[,8], col="red")
legend(x = "topleft", col = c("black", "red"), lty = 1,legend = c("LP H5N1", "HP H5N1"))

