---
title: "Week 3"
author: "Bas Doddema & Marco van Til"
date: "May 17, 2019"
output: pdf_document
---
## Assignment 1
Use of the median results in smaller differences in datapoints. The reason we don't use the mean is because the mean is more sensitive to outliers. When the drug dose is increased it results in a decreased concentration of receptor and a decreased concentration of receptor mRNA. This is because the activated receptor inhibits the synthesis of mRNA. The results of the model are not perfect in line with the experimental data. however, it can be seen that the medians show a small similarity. This may be due to the use of medians, which are not always reliable.
```{r}
# load data
data <- read.csv("MPL.csv", na.strings = "NA")

# calculate median MPL concentration per dose
median_MPL_01 <- median(data$MPL_conc[data$dose==0.1], na.rm=T)
median_MPL_03 <- median(data$MPL_conc[data$dose==0.3], na.rm=T)

# calculate median of MPL, mRNA and free receptor concentration
medians <- aggregate(data[,c("MPL_conc","mRNA","Free_receptor")],
                     list(data$dose,data$time), median, na.rm=T)
names(medians)[1:2] <- c("dose","time")
head(medians)

# set experimental variables per dose
MPL.01.EXP <- medians[medians$dose==0.1,]
MPL.03.EXP <- medians[medians$dose==0.3,]
```

```{r}
library(deSolve)

# define parameter fucntion
parms.func <- function(ks_rm = 2.90, ic50_rm = 26.2,
           kon = 0.00329, kt = 0.63, kre = 0.57,
           Rf = 0.49, kd_r = 0.0572, kd_rm = 0.612,
           ks_r = 3.22, D = 20){
  parms <- c(ks_rm = ks_rm, ic50_rm = ic50_rm,
           kon = kon, kt = kt, kre = kre,
           Rf = Rf, kd_r = kd_r, kd_rm = kd_rm,
           ks_r = ks_r, D = (D*1000)/374.471)
  return(parms)
}

# set parameters for 0.1 dose
parms.01 <- parms.func(D = median_MPL_01)

# set parameters for 0.3 dose
parms.03 <- parms.func(D = median_MPL_03)

# define initial state
state <- c(Rm0 = 4.74, R0 = 267, DR = 0, DR.N = 0)

# define time sequence for the model 
times <- seq(0, 168, by=1)

# define model 
rna.model <- function(t, y, parms) {
  with(as.list(c(parms, y)),{
         dmRNAr <- ks_rm * (1 - (DR.N/(ic50_rm+DR.N))) - kd_rm * Rm0
         dR <- ks_r * Rm0 + Rf * kre * DR.N - kon * D * R0 - kd_r * R0
         dDR <- kon * D * R0 - kt * DR
         dDR.N <- kt * DR - kre * DR.N
         return(list(c(dmRNAr, dR, dDR, dDR.N)))
       }
       )
}

# run simulations for both doses
model.01  <- ode(times = times, y = state, parms = parms.01, func = rna.model)
model.03  <- ode(times = times, y = state, parms = parms.03, func = rna.model)

# plot results of the model
plot(model.01[,2], ylab = "fmol/g", xlab="Time in hours", 
     main = "1. Concentration mRNA", type="l", ylim=range(0.5:5.5))
points(MPL.01.EXP[,2], MPL.01.EXP[,4], type="b", lty=2)
lines(model.03[,2],col="red")
points(MPL.03.EXP[,2], MPL.03.EXP[,4], col="red", type="b", lty=2)
legend(x="topright",lty=c(1,2), col = rep(c("black", "red"), each=2), 
       legend = c("model dose 0.1", "experiment dose 0.1", "model dose 0.3",
                  "experiment dose 0.3"))

# plot results of the model
plot(model.01[,3], ylab = "fmol/mg", xlab="Time in hours", 
     main = "2. Concentration free receptor", type="l", ylim=range(0:275))
points(MPL.01.EXP[,2], MPL.01.EXP[,5], type="b", lty=2)
lines(model.03[,3],col="red")
points(MPL.03.EXP[,2], MPL.03.EXP[,5], col="red", type="b", lty=2)
legend(x="topright",lty=c(1,2), col = rep(c("black", "red"), each=2), 
       legend = c("model dose 0.1", "experiment dose 0.1", "model dose 0.3",
                  "experiment dose 0.3"))
```


## Assignment 2

### 2.1
to retrieve the concentration activated drug-receptor complex without autoregulation of glucocorticoid, $DR.N/(ic50_rm+DR.N)$ should be removed in the formula. This is because $ic50_rm$ provides the autoregulation. 
In the following chunk the simulation is shown, the red line indicates the non-regulated receptor and the black line the regulated receptor. As you can see the first peak is equal for both states and the equilibrium of the unregulated is approximately 20, while this is 30 for the regulated. The equilibrium is lower because ic50_rm no longer has any effect on the transcription of receptor mRNA. Over time this results in a lower equilibrium of MPL recptor in the nucleus.
```{r}
# define a new model (without auto regulation)
rna.model.2 <- function(t, y, parms) {
  with(as.list(c(parms, y)),{
         dmRNAr <- ks_rm - kd_rm * Rm0
         dR <- ks_r * Rm0 + Rf * kre * DR.N - kon * D * R0 - kd_r * R0
         dDR <- kon * D * R0 - kt * DR
         dDR.N <- kt * DR - kre * DR.N
         return(list(c(dmRNAr, dR, dDR, dDR.N)))
       }
       )
}

parms <- parms.func()

# run simulations for both models
result.reg <- ode(times = times, y = state, parms = parms.01, func = rna.model.2)
result.unreg <- ode(times = times, y = state, parms = parms.01, func = rna.model)

# plot results of the models
plot(result.reg[,5], type = "l", ylab = "fmol/mg", xlab="Time in hours",
     main = "Concentration MPL receptor in nucleus")
lines(result.unreg[,5], col = "red")
legend(x = "topright", col = c("black", "red"), lty = 1, legend = c("regulated", "unregulated"))
```

### 2.2
When the steady state is reached (around 50), the drug treatment has been stopped and D has been set to zero. The concentration receptor mRNA will increase because the drug no longer stops transcription. Because there is more mRNA, there will be more translation and therefore more receptor.
```{r}
# results up to the steady state
res_drug <- ode(times = seq(0, 48, by = 1), y = state, func = rna.model, parms = parms)

# second initial state
steady <- res_drug[49, 2:5]

# set the drug parameter to zero
parms.no.drug <- parms.func(D = 0)

res_no_drug <- ode(times = seq(0, 100, by = 1), y = steady, func = rna.model,
                   parms = parms.no.drug)

plot(c(res_drug[, 2], res_no_drug[, 2]), main=c("mRNA concentration with drug set to zero at steady state"),
     cex.main=1, font.main=1, xlab="time in hours", ylab=c("fmol / g"),
     type="l", ylim=range(0:10))

plot(c(res_drug[, 3], res_no_drug[, 3]), main=c("Receptor concentration with drug set to zero at steady state"),
     cex.main=1, font.main=1, xlab="time in hours", ylab=c("fmol / g"), 
     type="l", ylim=range(0:275))

```

### 2.3
To look at the effect of changing $Kon$ and $Kre$, the parameters are changed with a self made function. There are a 2 and 5 times decrease and increase relative to the basic values. The decrease of $Kon$ results in a increase of receptor mRNA, this is because with a decrease of $Kon$, there will be less activated receptor and also less $ic50_rm$ which again results in less inhibition and therefore more mRNA. The opposite applies to an increase of $Kon$. There will be an increase of activated receptor and $ic50_rm$, so there will be more inhibition and less mRNA.
A decrease of $Kre$ results in more breakdown and recycle of the activated receptor in the cytosol. This leads to a decrease of $ic50_rm$ and less inhibition of transcription. In the end there will be more mRNA when $Kre$ decreases. When $Kre$ increases, the opposite happens, mRNA decreases.
```{r}
# define parameters
kon.5.div <- parms.func(kon = 0.00329/5)
kon.2.div <- parms.func(kon = 0.00329/2)
kon.5.tim <- parms.func(kon = 0.00329*5)
kon.2.tim <- parms.func(kon = 0.00329*2)

# run simulations for the different parameters 
basic.model <- ode(times = times, y = state, parms = parms, func = rna.model)
kon.5.div.model <- ode(times = times, y = state, parms = kon.5.div, func = rna.model)
kon.2.div.model <- ode(times = times, y = state, parms = kon.2.div, func = rna.model)
kon.5.tim.model <- ode(times = times, y = state, parms = kon.5.tim, func = rna.model)
kon.2.tim.model <- ode(times = times, y = state, parms = kon.2.tim, func = rna.model)

# plot results of the model mRNA
plot(basic.model[,2], type = "l", ylab = "fmol/mg", xlab="Time in hours",
     main = "Concentration mRNA with increase and decrease of Kon", ylim = range(0:10))
lines(kon.5.div.model[,2], col = "red")
lines(kon.2.div.model[,2], col = "blue")
lines(kon.5.tim.model[,2], col = "purple")
lines(kon.2.tim.model[,2], col = "green")
legend(x="topright", legend=c("control", "times 5 decrease", "times 2 decrease",
                              "times 5 increase", "times 2 increase"), 
       cex=1, col=c("black", "red", "blue", "purple", "green"), lty=1)

# plot results of the model receptor
plot(basic.model[,3], type = "l", ylab = "fmol/mg", xlab="Time in hours",
     main = "Concentration receptor with increase and decrease of Kon", ylim = range(0:275))
lines(kon.5.div.model[,3], col = "red")
lines(kon.2.div.model[,3], col = "blue")
lines(kon.5.tim.model[,3], col = "purple")
lines(kon.2.tim.model[,3], col = "green")
legend(x="topright", legend=c("control", "times 5 decrease", "times 2 decrease",
                              "times 5 increase", "times 2 increase"), 
       cex=1, col=c("black", "red", "blue", "purple", "green"), lty=1)
```

### 2.3
```{r}
# define parameters
kre.5.div <- parms.func(kre = 0.57/5)
kre.2.div <- parms.func(kre = 0.57/2)
kre.5.tim <- parms.func(kre = 0.57*5)
kre.2.tim <- parms.func(kre = 0.57*2)

# run simulations for the different parameters 
basic.model <- ode(times = times, y = state, parms = parms, func = rna.model)
kre.5.div.model <- ode(times = times, y = state, parms = kre.5.div, func = rna.model)
kre.2.div.model <- ode(times = times, y = state, parms = kre.2.div, func = rna.model)
kre.5.tim.model <- ode(times = times, y = state, parms = kre.5.tim, func = rna.model)
kre.2.tim.model <- ode(times = times, y = state, parms = kre.2.tim, func = rna.model)

# plot results of the model mRNA
plot(basic.model[,2], type = "l", ylab = "fmol/mg", xlab="Time in hours",
     main = "Concentration mRNA with increase and decrease of Kre", ylim = range(0.5:5))

# add a legend and colors
lines(kre.5.div.model[,2], col = "red")
lines(kre.2.div.model[,2], col = "blue")
lines(kre.5.tim.model[,2], col = "purple")
lines(kre.2.tim.model[,2], col = "green")
legend(x="topright", legend=c("control", "times 5 decrease", "times 2 decrease",
                              "times 5 increase", "times 2 increase"), 
       cex=1, col=c("black", "red", "blue", "purple", "green"), lty=1)

# plot results of the model receptor
plot(basic.model[,3], type = "l", ylab = "fmol/mg", xlab="Time in hours",
     main = "Concentration receptor with increase and decrease of Kre", ylim = range(0:275))

# add a legend and colors
lines(kre.5.div.model[,3], col = "red")
lines(kre.2.div.model[,3], col = "blue")
lines(kre.5.tim.model[,3], col = "purple")
lines(kre.2.tim.model[,3], col = "green")
legend(x="topright", legend=c("control", "times 5 decrease", "times 2 decrease", 
                              "times 5 increase", "times 2 increase"), 
       cex=1, col=c("black", "red", "blue", "purple", "green"), lty=1)
```

### 2.4
To see what happens when the synthesis of the receptor is completely blocked, the speed constant that takes care of the synthesis of receptor mRNA; $ks_r$ must be set to zero. All the concentrations will go back to their initial value because there is no translation and therefore no receptor.
```{r}
# define parameters (without synthesis)
parms.no.synt <- parms.func(ks_r = 0)

# run simulations for the different parameters
model.no.synt <- ode(times = times, y = state, parms = parms.no.synt,
                     func = rna.model, method = "euler")

# plot results of the model
plot(model.no.synt, ylab = c("fmol/g", "fmol/mg", "fmol/mg", "fmol/mg"), xlab="Time in hours", 
     main = c("Concentration mRNA", "Concentration receptor", "Concentration MPL receptor",
              "Concentration MPL receptor in nucleus"))
```

### 2.5
A decrease of ks_rm and kd_rm results in less synthesis and less breakdown of the mRNA. The times 5 decreases less fast. This is because there is less inhibition of synthesis of mRNA. The times 5 increase drops faster because there is more inhibition. The receptor concentration decreases gradually and is about the same for all states. This is because more and more is used for the production of activated receptor.
```{r}
# define parameters
parms.div.5 <- parms.func(ks_rm = 2.9/5, kd_rm = 2.9/5/4.74)
parms.div.2 <- parms.func(ks_rm = 2.9/2, kd_rm = 2.9/2/4.74)
parms.tim.5 <- parms.func(ks_rm = 2.9*5, kd_rm = 2.9*5/4.74)
parms.tim.2 <- parms.func(ks_rm = 2.9*2, kd_rm = 2.9*2/4.74)

# run simulations for the different parameters
parms.div.5.model <- ode(times = times, y = state, parms = parms.div.5, func = rna.model)
parms.div.2.model <- ode(times = times, y = state, parms = parms.div.2, func = rna.model)
parms.tim.5.model <- ode(times = times, y = state, parms = parms.tim.5, func = rna.model)
parms.tim.2.model <- ode(times = times, y = state, parms = parms.tim.2, func = rna.model)

# plot results of the model (mRNA)
plot(basic.model[,2], type = "l", ylab = "fmol/mg", xlab="Time in hours",
     main = "Concentration mRNA with increase and decrease of ks_rm and kd_rm", ylim = range(1:5))
lines(parms.div.5.model[,2], col = "red")
lines(parms.div.2.model[,2], col = "blue")
lines(parms.tim.5.model[,2], col = "purple")
lines(parms.tim.2.model[,2], col = "green")
legend(x="topright", legend=c("control", "times 5 decrease", "times 2 decrease",
                              "times 5 increase", "times 2 increase"), 
       cex=1, col=c("black", "red", "blue", "purple", "green"), lty=1)

# plot results of the model (receptor)
plot(basic.model[,3], type = "l", ylab = "fmol/mg", xlab="Time in hours",
     main = "Concentration receptor with increase and decrease of ks_rm and kd_rm", ylim = range(50:300))
lines(parms.div.5.model[,3], col = "red")
lines(parms.div.2.model[,3], col = "blue")
lines(parms.tim.5.model[,3], col = "purple")
lines(parms.tim.2.model[,3], col = "green")
legend(x="topright", legend=c("control", "times 5 decrease", "times 2 decrease",
                              "times 5 increase", "times 2 increase"), 
       cex=1, col=c("black", "red", "blue", "purple", "green"), lty=1)
```

