---
title: "Untitled"
author: "Bas Doddema"
date: "April 23, 2019"
output:
  html_document: default
  pdf_document: default
---


[1] Hoe hoger de pV, hoe sneller de evenwichtstatus bereikt word. Hoe hoger addVolume, hoe hoger het evenwicht.
[2] Zolang de initial state onder de addVolume waarde blijft verandert er niks.
[3] De grafiek word minder smooth wanneer de by waarde aangepast wordt.
[4] Als de addVolumde weg gehaald word komt de evenwichtstatus altijd op 100 uit.




```{r}
library(deSolve)

parameters <- c(addVolume = 10, pV = 0.1) 

# define model 
volume <- function(t,y,parms){
  with(as.list(c(parms)),{
         dY <- addVolume - pV * (y)
         return(list(c(dY)))
       }
       )
}
#initial state
state <- c(Volume = 0)

#define time sequence you want to run the model
times <- seq(0, 100,  by = 1)

# run simulation using continuous approach
out  <- ode(times = times, y = state,   parms = parameters, func = volume, method = "euler")

plot(out)
```


```{r}
parameters <- c(a = -8/3,
b = -10,
c = 28)

state <- c(X = 1,
Y = 1,
Z = 1)

Lorenz<-function(t, state, parameters) {
with(as.list(c(state, parameters)),{
# rate of change
dX <- a*X + Y*Z
dY <- b * (Y-Z)
dZ <- -X*Y + c*Y - Z
# return the rate of change
list(c(dX, dY, dZ))
}) # end with(as.list ...
}

times <- seq(0, 100, by = 0.01)

out <- ode(y = state, times = times, func = Lorenz, parms = parameters)
head(out)

par(oma = c(0, 0, 3, 0))
plot(out, xlab = "time", ylab = "-")
plot(out[, "X"], out[, "Z"], pch = ".")
mtext(outer = TRUE, side = 3, "Lorenz model", cex = 1.5)
```




```{r}
combustion <- function (t, y, parms)
list(y^2 * (1-y) )
yini <- 0.01
times <- 0 : 200

out <- ode(times = times, y = yini, parms = 0, func = combustion)
out2 <- ode(times = times, y = yini*2, parms = 0, func = combustion)
out3 <- ode(times = times, y = yini*3, parms = 0, func = combustion)
out4 <- ode(times = times, y = yini*4, parms = 0, func = combustion)

plot(out, out2, out3, out4, main = "combustion")
legend("bottomright", lty = 1:4, col = 1:4, legend = 1:4, title = "yini*i")
```

```{r}
obs <- subset (ccl4data, animal == "A", c(time, ChamberConc))
names(obs) <- c("time", "CP")

parms <- c(0.182, 4.0, 4.0, 0.08, 0.04, 0.74, 0.05, 0.15, 0.32, 16.17,
           281.48, 13.3, 16.17, 5.487, 153.8, 0.04321671,
           0.40272550, 951.46, 0.02, 1.0, 3.80000000)
yini <- c(AI = 21, AAM = 0, AT = 0, AF = 0, AL = 0, CLT = 0, AM = 0)
out <- ccl4model(times = seq(0, 6, by = 0.05), y = yini, parms = parms)
par2 <- parms
par2[1] <- 0.1
out2 <- ccl4model(times = seq(0, 6, by = 0.05), y = yini, parms = par2)
par3 <- parms
par3[1] <- 0.05
out3 <- ccl4model(times = seq(0, 6, by = 0.05), y = yini, parms = par3)

plot(out, out2, out3, which = c("AI", "MASS", "CP"),
col = c("black", "red", "green"), lwd = 2,
obs = obs, obspar = list(pch = 18, col = "blue", cex = 1.2))
legend("topright", lty = c(1,2,3,NA), pch = c(NA, NA, NA, 18),
col = c("black", "red", "green", "blue"), lwd = 2,
legend = c("par1", "par2", "par3", "obs"))
```

```{r}
hist(out, col = grey(seq(0, 1, by = 0.1)), mfrow = c(3, 4))
```





